import numpy as np
import nibabel as nib
import os
from fsl_pad import pad_image

def test_pad_image():
    # the input image (IXI subject) has shape (180, 181, 217)
    input_image = 'in.nii.gz'
    temp_output_image = 'temp_output.nii.gz'
    
    # Call pad_image to pad to 256x256x256
    pad_image(input_image, temp_output_image, 256, 256, 256)
    
    # Load the padded image and check its shape
    padded_img = nib.load(temp_output_image).get_fdata()
    assert padded_img.shape == (256, 256, 256)
    
    # Cleanup temporary files
    os.remove(temp_output_image)
