# NIfTI Image Padder

This project provides a Python CLI utility to pad 3D NIfTI images to a desired size. The utility ensures that the padding is symmetrically added to both sides of each dimension, and it applies zero-padding.

Additionally, this script preprocesses the image by cropping using FSL's `robustfov` and reorienting the image to RAS orientation.

## Dependencies

- nibabel
- numpy
- fslpy
- FSL (for the robustfov utility)

## Installation

Clone the repository and ensure you have the required dependencies:

```bash
conda install -c conda-forge nibabel numpy pytest
```

Make sure you also have [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation) installed and correctly set up.

## Usage

```bash
fslpython fsl_pad.py -input [input_path] -output [output_path] -x [x_dimension] -y [y_dimension] -z [z_dimension]
```

### Parameters:

- `-input`: Path to the input NIfTI image file that needs to be padded.
- `-output`: Path where the padded NIfTI image will be saved.
- `-x`: Desired size along the x-dimension after padding.
- `-y`: Desired size along the y-dimension after padding.
- `-z`: Desired size along the z-dimension after padding.

### Example:

To pad an image named `brain.nii.gz` to a size of 256x256x256:

```bash
python fsl_pad.py -input brain.nii.gz -output brain_padded.nii.gz -x 256 -y 256 -z 256
```

If the image is already of the desired size, a message will be printed and no action will be performed.

## Testing

To run the tests, simply run:

```bash
pytest
```

## License

[FSL License](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Licence)