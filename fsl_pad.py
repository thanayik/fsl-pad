#!/usr/bin/env fslpython

import argparse
import os
import nibabel as nib
import numpy as np
from fsl.wrappers.bet import robustfov

def pad_image(input_image, output_image, x_size, y_size, z_size):
    '''
    Pads a given 3D NIfTI image to a specified size. If the image is already 
    of the desired size, a message is printed and no action is performed. 
    The padding is symmetrically added to both sides of each dimension, 
    and zero-padding is applied.

    Parameters:
    - input_image (str): Path to the input NIfTI image file that needs to be padded.
    - output_image (str): Path where the padded NIfTI image will be saved.
    - x_size (int): Desired size along the x-dimension after padding.
    - y_size (int): Desired size along the y-dimension after padding.
    - z_size (int): Desired size along the z-dimension after padding.

    Returns:
    None. The padded image is saved to the specified output path.

    '''
    # Load the NIfTI image
    nii = nib.load(input_image)
    # reorient to RAS  
    nii = nib.as_closest_canonical(nii)
    img = nii.get_fdata()

    # Check if the image is already the desired size
    if img.shape == (x_size, y_size, z_size):
        print(f"The input image is already of size {x_size}x{y_size}x{z_size}.")
        return

    # Calculate the padding sizes for each dimension
    pad_width_x = x_size - img.shape[0]
    pad_width_y = y_size - img.shape[1]
    pad_width_z = z_size - img.shape[2]
    
    pad_width = (
        (pad_width_x // 2, pad_width_x - pad_width_x // 2),
        (pad_width_y // 2, pad_width_y - pad_width_y // 2),
        (pad_width_z // 2, pad_width_z - pad_width_z // 2),
    )

    # Pad the image
    padded_img = np.pad(img, pad_width, mode='constant')

    # Create a new NIfTI image with the updated data and the same header
    padded_nii = nib.Nifti1Image(padded_img, nii.affine, nii.header)

    # Save the padded NIfTI image
    nib.save(padded_nii, output_image)

def main():
    '''
    Run the required actions to pad an input image to the requested size.
    '''
    # setup the command line arguments parser
    parser = argparse.ArgumentParser(description="Pad a 3D NIfTI image to a given size. Note: the padding is applied to the image after it has been cropped using FSL's robust fov and reoriented to RAS.")
    # add the arguments 
    parser.add_argument("-input", help="Path to the input NIfTI image.")
    parser.add_argument("-output", help="Path where the padded NIfTI image will be saved.")
    parser.add_argument("-x", type=int, help="Size of the x dimension after padding.")
    parser.add_argument("-y", type=int, help="Size of the y dimension after padding.")
    parser.add_argument("-z", type=int, help="Size of the z dimension after padding.")

    args = parser.parse_args()

    # Use the arguments
    input_image = args.input
    output_image = args.output
    if output_image is None:
        output_image = input_image.split('.nii')[0] + '_padded.nii.gz'
    x_size = args.x
    y_size = args.y
    z_size = args.z

    # remove the extension from the input image
    input_base = input_image.split('.nii')[0]

    # robust out image
    r_out = f'{input_base}_robust.nii.gz'
    robustfov(input_image, r_out)

    # pad the robust image
    pad_image(r_out, output_image, x_size, y_size, z_size)

    # remove the robust image
    os.remove(r_out)

if __name__ == '__main__':
    # run the main function
    main()